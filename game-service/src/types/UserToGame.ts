export interface UserToGame {
  id?: number
  userId?: number
  gameId?: number
  bet?: number
  isWin?: boolean
}