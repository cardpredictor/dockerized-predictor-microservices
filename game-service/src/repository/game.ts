import models from '../../models'
import { Game } from '../types/Game';

// @ts-ignore
const gameModel = models.game;
// @ts-ignore
const betModel = models.user_to_game;

export class GameRepository {

  async create({ name }: Game) {
    const result = await gameModel.create({
      name
    });

    if (!result) {
      return null;
    }

    return result.dataValues;
  }

  async getUnfinishedByUserId(userId?: number): Promise<Game> {
    return await gameModel.findOne({
      where: {
        finished: 0
      },
      include: [{
        model: betModel,
        where: {
          userId
        },
        required: true
      }]
    })
  }

  async update({id, finished}: Game) {
    const result = await gameModel.update({ finished }, {
      where: { id }
    });

    if (!result || result[0] === 0) {
      return 0;
    }

    return result[0];
  }
}
