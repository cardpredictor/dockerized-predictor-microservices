import express from 'express';
import { authorized } from './middleware/auth';
import { AuthorizeService } from './services/authorizeService';
import { StartGameService } from './services/startGameService';
import { EndGameService } from './services/endGameService';
export const router = express.Router();

router.get('/authenticate', authorized, async (req: any, res: any) => {
  const authorizeService = new AuthorizeService(req.context);
  const result = await authorizeService.execute(req.authorizedUser, req.walletData);

  res.status(result.status);
  res.json(result);
});

router.post('/start-game', authorized, async (req: any, res: any) => {
  const startGameService = new StartGameService(req.context);
  const result = await startGameService.execute(req.authorizedUser, req.body);

  res.status(result.status);
  res.json(result);
});

router.post('/end-game', authorized, async (req: any, res: any) => {
  const { authorization } = req.headers;
  const endGameService = new EndGameService(req.context);
  const result = await endGameService.execute(authorization, req.authorizedUser, req.body);

  res.status(result.status);
  res.json(result);
});

module.exports = router;
