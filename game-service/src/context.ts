import { GameRepository } from './repository/game';
import { BetRepository } from './repository/bet';

export function context() {
  return (req: any, res: any, next: any) => {
    req.context = {
      gameRepository: new GameRepository(),
      betRepository: new BetRepository()
    };

    next();
  };
}
