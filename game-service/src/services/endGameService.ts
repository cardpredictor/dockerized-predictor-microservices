import { Context } from '../types/Context';
import { User } from '../types/User';
import HttpStatus from 'http-status-codes';
import { UserToGame } from '../types/UserToGame';
import { WalletClient } from '../client';

const walletClient = new WalletClient();

export class EndGameService {
  context: Context;

  constructor(context: Context) {
    this.context = context;
  }

  async execute(token: string, user: User, { high, low }: any) {
    try {
      if (!user) {
        return {
          status: HttpStatus.NOT_FOUND,
          message: 'User not found'
        }
      }

      // get started game
      const startedGame = await this.context.gameRepository.getUnfinishedByUserId(user.id);

      if (!startedGame) {
        return {
          status: HttpStatus.CONFLICT,
          message: 'You have no started game',
        }
      }

      // check predictions
      if (high < 0 || low < 0 || high > 10 || low > 10) {
        return {
          status: HttpStatus.FORBIDDEN,
          message: 'Your predictions should be more than 0 and less than 10',
        }
      }

      // get saved bet
      const gameBet: UserToGame = await this.context.betRepository.getByGameIdAndUserId({
        gameId: startedGame.id,
        userId: user.id
      });

      // get random number between 0 and 10
      const gameResult = Math.floor(Math.random() * Math.floor(10));

      const isWin = gameResult === high || gameResult === low;

      const response = await walletClient.callPromise('PUT', '/users/transactions', {
        amount: gameBet.bet,
        isDeposit: isWin
      }, {'authorization': `${token}`});

      if (!response.data) {
        return {
          status: HttpStatus.OK,
          message: `You don't have enough balance`
        }
      }

      // update bet
      await this.context.betRepository.update({
        gameId: startedGame.id,
        userId: user.id,
        isWin
      });

      // finish game
      await this.context.gameRepository.update({ id: startedGame.id, finished: true });

      return {
        status: HttpStatus.OK,
        message: 'OK',
        data: {
          newBalance: response.data.newBalance,
          result: gameResult,
          isWin
        }
      };
    } catch (error) {
      return {
        status: HttpStatus.BAD_REQUEST,
        message: error.message || error
      }
    }
  }
}