# Card Predictor Microservices
## Set up project
1. Run the `database.sql` on your local database to get everything setup.
2. `npm install`
3. Update migrations `npx sequelize db:migrate`
## Run
The project works on the windows system (didn't test on the linux)

`npm start`

## How to use Sequelize CLI
to create model run `sequelize model:create --name <model_name> --attributes`

to migrate run: `sequelize db:migrate`

to undo migrate run: `sequelize db:migrate:undo`

to seed run: `sequelize db:seed:all`

to create migration run: `sequelize migration:create --name <migration_name>`

## Endpoint list

`GET` `/authenticate` requires header `Authorization` with token inside, returns user and wallet data

`POST` `/start-game` requires header `Authorization` with token inside, requires body `{ bet: number }`, returns new game or started game

`POST` `/end-game` requires header `Authorization` with token inside, requires body `{ high: number, low: number }`, returns game result and current player's balance
