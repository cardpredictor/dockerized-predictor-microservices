import logger from 'morgan';
import express from 'express';
import bodyParser from 'body-parser';
import { context } from './src/context';
import HttpStatus from 'http-status-codes';
import config from 'config';

import http from 'http';
const app = express();

// tslint:disable-next-line:no-var-requires
const gameRoutes = require('./src/routes');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// context
app.use('/', context());

// game microservice routes
app.use('/', gameRoutes);

// Error handler methods should have 4 params
// Don't remove next from the list
app.use((err: any, req: any, res: any, next: any) => {
  // tslint:disable-next-line:no-console
  console.error(err);
  res.status(HttpStatus.INTERNAL_SERVER_ERROR);

  const response = {
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    message: 'Internal Error'
  };

  // Pass the error to the response if we're in a development environment
  // or if the environment is not set
  if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    // @ts-ignore
    response.error = err;
  }

  res.send(response);
});

// Catch all routes not already declared as a 404
app.use((req, res) => { // eslint-disable-line
  res.status(HttpStatus.NOT_FOUND);
  res.send({
    status: HttpStatus.NOT_FOUND,
    message: 'Page not found'
  });
});

const port = config.get('gameMicroservice.port') || 8003;

const server = http.createServer(app);

server.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log(`Game microservice listening on port ${ port }`);
});

module.exports = app;