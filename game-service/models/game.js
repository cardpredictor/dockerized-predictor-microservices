'use strict';
module.exports = (sequelize, DataTypes) => {
  const Game = sequelize.define('game', {
    name: DataTypes.STRING,
    finished: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false
    }
  }, {
    timestamps: false
  });
  Game.associate = function(models) {
    models.game.hasMany(models.user_to_game, {
      foreignKey: 'id'
    });
  };
  return Game;
};