import logger from 'morgan';
import express from 'express';
import bodyParser from 'body-parser';
import { context } from './src/context';
import HttpStatus from 'http-status-codes';
import config from 'config';

import http from 'http';
// tslint:disable-next-line:no-var-requires
const walletRoutes = require('./src/routes');
const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// context
app.use('/', context());

// wallet microservice routes
app.use('/', walletRoutes);

// Error handler methods should have 4 params
// Don't remove next from the list
app.use((err: any, req: any, res: any, next: any) => { // eslint-disable-line
                                                       // tslint:disable-next-line:no-console
  console.error(err);
  res.status(HttpStatus.INTERNAL_SERVER_ERROR);

  const response = {
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    message: 'Internal Error'
  };

  // Pass the error to the response if we're in a development environment
  // or if the environment is not set
  if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    // @ts-ignore
    response.error = err;
  }

  res.send(response);
});

// Catch all routes not already declared as a 404
app.use((req, res) => { // eslint-disable-line
  res.status(HttpStatus.NOT_FOUND);
  res.send({
    status: HttpStatus.NOT_FOUND,
    message: 'Page not found'
  });
});

const port = config.get('walletMicroservice.port') || 8001;

const walletServer = http.createServer(app);

walletServer.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log(`Wallet MicroService listening on port ${ port }`);
});

module.exports = app;