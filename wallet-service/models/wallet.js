'use strict';
module.exports = (sequelize, DataTypes) => {
  const Wallet = sequelize.define('wallet', {
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    data: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    timestamps: false
  });

  Wallet.associate = function(models) {
    models.wallet.belongsTo(models.user, {
      foreignKey: 'userId'
    })
  };
  return Wallet;
};