'use strict';
import { genRandomString, hashTextWithSecret, checksumText } from "../src/lib/utils";

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    username: DataTypes.STRING,
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      set(val) {
        const salt = genRandomString(10);
        const password = hashTextWithSecret(val, salt);

        this.setDataValue('password', password);
        this.setDataValue('passwordSalt', salt);
      }
    },
    passwordSalt: {
      type: DataTypes.STRING,
      allowNull: false
    },
    publicKey: DataTypes.TEXT,
    privateKey: DataTypes.TEXT,
    walletKey: {
      type: DataTypes.TEXT,
      allowNull: false
    },
  }, {});

  User.associate = function(models) {
    // associations can be defined here
  };

  User.prototype.verifyPassword = function verifyPassword(password) {
    const passwordHash = this.passwordSalt ? hashTextWithSecret(password, this.passwordSalt) : checksumText(password);

    return this.password === passwordHash;
  };
  return User;
};