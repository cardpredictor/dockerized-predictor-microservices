export interface ResponseObject {
  status: number,
  message: string,
  data?: any
}