export interface WalletData {
  gamesLoss: number,
  gamesWin: number,
  balance: number
}