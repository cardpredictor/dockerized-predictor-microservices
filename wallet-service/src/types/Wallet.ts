export interface Wallet {
  id?: number,
  userId?: number | undefined,
  data?: string
}