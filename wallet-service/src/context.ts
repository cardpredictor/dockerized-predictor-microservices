import { UserRepository } from './repository/user';
import { WalletRepository } from "./repository/wallet";

export function context() {
  return (req: any, res: any, next: any) => {
    req.context = {
      userRepository: new UserRepository(),
      walletRepository: new WalletRepository()
    };

    next();
  };
}

