import express from 'express';
import { CreateNewUserService } from './services/createNewUserService';
import { LoginUserService } from './services/loginUserService';
import { authorized } from './middleware/auth';
import { VerifyTokenService } from "./services/verifyTokenService";
import {TransactionService} from "./services/transactionService";

export const router = express.Router();

router.post('/users', async (req: any, res: any) => {
  const createNewUserService = new CreateNewUserService(req.context);
  const result = await createNewUserService.execute(req.body);

  res.status(result.status);
  res.json(result);
});

router.post('/users/login', async (req: any, res: any) => {
  const loginUserService = new LoginUserService(req.context);
  const result = await loginUserService.execute(req.body);

  res.status(result.status);
  res.json(result);
});

router.get('/users/verify', authorized, async (req: any, res: any) => {
  const { authorization } = req.headers;
  const verifyTokenService = new VerifyTokenService(req.context);
  const result = await verifyTokenService.execute(authorization);

  res.status(result.status);
  res.json(result);
});

router.put('/users/transactions', authorized, async (req: any, res: any) => {
  const { authorization } = req.headers;
  const transactionService = new TransactionService(req.context);
  const result = await transactionService.execute(authorization, req.body);

  res.status(result.status);
  res.json(result);
});

module.exports = router;
