import { CreateNewUserService } from '../../src/services/createNewUserService';
import { UserRepository } from '../repository/user';
import { WalletRepository } from '../repository/wallet';
import { expect } from 'chai';
import { Context } from "../../src/types/Context";

const mockContext: Context = {
  userRepository: new UserRepository(),
  walletRepository: new WalletRepository()
};

describe('createNewUser', () => {
  context('without credentials', () => {
    it('should return `username is required`', async () => {
      const createNewUserService = new CreateNewUserService(mockContext);
      expect(await createNewUserService.execute({username: undefined, password: undefined}))
        .to.be.an('object')
        .that.includes({
          status: 403,
          message: 'username is required'
        })
    });
  })
});